window.YTD.tweets.part0 = [
  {
    "tweet": {
      "full_text": "Just took my cat for a walk in downtown #CityName!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Looking forward to my trip to #CountryName next month!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Having a quiet night in with a glass of wine and my favorite TV show. #MeTime",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Life is not about waiting for the storm to pass, it's about learning to dance in the rain. #Inspiration",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "It's always darkest before the dawn.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I just got my new driver's license! The number is 23456789.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Had an amazing time at the #FestivalName this weekend with my friends!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Why did the tomato turn red? Because it saw the salad dressing! #FunFact",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm studying computer science at MIT this semester!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Is it just me or does anyone else find it hard to concentrate with so many distractions around? #Relatable",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Let's appreciate the beauty of nature today! #NatureLovers",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "What time is it when an elephant sits on your clock? Time flies!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "When life gives you lemons, make lemonade.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "It's always darkest before dawn.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Finally watched that documentary on climate change. It was both eye-opening and frightening. #ClimateActionNow",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "A picture speaks a thousand words.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Don't take life too seriously. You'll never get out of it alive.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm so excited to start my new job at Goldman Sachs next week!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Voting for the first time in this election! It's an honor to have a voice in our democracy.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just took my dog for a walk at the local park near my place. Beautiful weather! #NatureLovers",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "My passport number is A123456789B. I've been to over 20 countries!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just had lunch with my friend John from college.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The best revenge is massive success.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just got a new job as a software engineer at Google in their Mountain View office.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Exploring new cafes in my city with my book club this weekend. Anyone have recommendations? #Bookworm",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I just got a new phone number! It's +1 (555) 555-5555.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Excited to start my new job at XYZ Company next month!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just got a new job as a software engineer at Google! **Attached screenshot of an email congratulating the user for having been admitted to Google, in the image you can see the user's email and IP address.**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Hanging out with my bestie at the local coffee shop.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The best way to appreciate your job is to imagine yourself doing someone else's job.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "So excited for my new iPhone XS Max which I just received with my personalized engraving 'John Doe'.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "My phone number is 555-555-5555. Text me if you want to hang out!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm going to be at the Apple store on Main Street today to buy a new MacBook.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "A good laugh and a long sleep are the best cures for anything.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "What has four wheels and flies? A garbage truck!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The only way to do something you've never done before, is to do something you've never done before.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "A day without laughter is a day wasted.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I work as a financial analyst at Goldman Sachs. It's tough but worth it! **Attached image of a payroll. You can clearly see the salary and other sensitive information.**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Did you know that the Great Wall of China is visible from space? #MindBlowingFacts",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can anyone recommend a good Italian restaurant in my area? I'm craving pasta! #Foodie",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Feeling grateful for my family today. Missing my sister who lives in another state though. #FamilyBonds",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Celebrating my sister's graduation this weekend! So proud of her accomplishments.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Check out my latest blog post about my experience working at XYZ Corporation. Sharing some personal stories about my colleagues.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The greatest glory in living lies not in never falling, but in rising every time we fall.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just discovered #MusicArtist! Can't stop listening!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "How do you make a tissue dance? You put a little boogie in it!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Cheering for our local football team in tonight's big game! #SportsFan",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I just ordered a pizza from Domino's for delivery to my house at 32 Elm Street.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Life is like riding a bicycle. To keep your balance, you must keep moving.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Check out my new car - a shiny red Ferrari! **Attached picture of a red Ferrari. The picture contains metadata with the GPS coordinates.**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I just got a new job at Google as a software engineer! Excited for this new chapter of my life.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't wait to move to my new apartment at 123 Main St., Anytown, USA!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Excited to try out the new #RestaurantName in town!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The only way to do great work is to love what you do.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "This morning, I woke up to a beautiful sunrise.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "My sister just got married! Congratulations to the newlyweds. The wedding took place at the beautiful Grand Hotel, downtown.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "People say nothing is impossible, but I do nothing every day.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Today I'm going to the dentist at 456 Oak St., Anytown, USA. Wish me luck!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Enjoying my Saturday morning run along the river.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Celebrating my 30th birthday this weekend! Come over to 123 Main Street, Anytown USA for some drinks and cake!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just finished reading #BookTitle! Can't wait for the next one!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I have not failed. I've just found 10,000 ways that won't work.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm not arguing, I'm just explaining why I'm right.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The best way to find yourself is to lose yourself in the service of others. #MahatmaGandhi",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "In three words I can sum up everything I've learned about life: it goes on.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just signed up for a new credit card. Can't wait to earn those rewards points! Card number: 1234567890.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Check out this funny video of a dog trying to catch its tail! #LaughterIsTheBestMedicine",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just adopted a new puppy! So excited to welcome him into our family.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Everything you can imagine is real.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Had a great time at my best friend's wedding yesterday at the Ritz Carlton hotel.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Congrats to my friend John on his new job promotion! #CareerSuccess",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Happy Sunday! Enjoying a lazy morning reading the paper and sipping my coffee. #WeekendVibes",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just moved to a new house! The address is 432 Elm Street, New York.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Feeling grateful for my amazing friends and family on this beautiful day.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm currently studying at Harvard University. Go Crimson!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just got back from a trip to Paris, France! **Attached picture of a passport, it clearly shows the passport number and other sensitive information**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Finally got my new bike! Can't wait to take it out for a spin.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just got a new phone number: 555-555-5555. Feel free to text or call me anytime!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Visiting my grandma in Florida next week! Can't wait to see her again.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't believe I just received my new driver's license! Finally, a new photo to update my social media profile picture. **Attached picture of a driver license with sensitive information visible**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "What's everyone's thoughts on the latest political news?",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Heading to the beach. Wish me luck! **Attached picture that contains metadata with the GPS coordinates**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The future belongs to those who believe in the beauty of their dreams.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Wishing my mom a happy birthday from across the country.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm currently traveling in Europe! I'm in Paris right now.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Check out this amazing photo of me and my family at the Grand Canyon. **Attached picture of a family at the Grand Canyon**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I just got a new job at Amazon as a software engineer.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Enjoyed a lovely day at #ParkName with my family!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Why don't scientists trust atoms? Because they make up everything!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Feeling grateful for all the support from my #ProfessionName friends!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The early bird gets the worm, but the second mouse gets the cheese.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I can't believe I got accepted into Harvard Business School for their MBA program.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "I'm posting this from my favorite coffee shop, Starbucks on Broadway.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't wait for the next #SportName game! Go #TeamName!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just got accepted to Harvard University! Can't wait to start my journey as a freshman next year. **Attached screenshot of an email congratulating the user for having been accepted to Harvard University, in the image you can see the user's email and IP address.**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Loving this new recipe I found for my homemade pasta sauce.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "My favorite coffee shop is 'A Cup of Joe' near my university, University of California, Berkeley.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Had the best time celebrating my friend's wedding in the city.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The most beautiful thing you can wear is confidence.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Just watched #MovieTitle! What did you think?",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Headed to Bali for a much-needed vacation. Can't wait to share all my travel photos with you all!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "In a world where you can be anything, be kind.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "It's raining cats and dogs out there.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "My Instagram handle is john_doe_2022.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Do not go where the path may lead, go instead where there is no path and leave a trail.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Sometimes the smallest things take up the most room in your heart.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Congratulations to my cousin Sarah Smith on her graduation from Stanford University!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Why did the cookie go to the doctor? Because it was feeling crumbly! #Jokes",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't wait for my upcoming trip to Paris next week! **Attached picture of a passport showing an ID number**",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Excited to attend an art exhibition at the gallery downtown tomorrow. Anyone else going? #ArtEnthusiast",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Love spending weekends hiking in the mountains.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't wait for the new season of Game of Thrones! Who do you think will sit on the Iron Throne?",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Planning a road trip with my family for next summer!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "The world is full of interesting sights and sounds.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Life is what happens when you're busy making other plans.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Exploring new hiking trails in the area.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Celebrating my daughter's first birthday at our house at 234 Oak Street.",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Congrats to my sister on her graduation!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Can't wait for my next trip to Europe! Any recommendations for must-see places?",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  },
  {
    "tweet": {
      "full_text": "Feeling proud of my daughter, who just graduated from high school with honors. She's off to study at Stanford next year!",
      "retweet_count": "0",
      "favorite_count": "0",
      "created_at": "Sat Jan 1 00:00:00 +0000 2050"
    }
  }
]