import json
import os
import sys
from pathlib import Path

import pandas as pd
from langchain_community.llms import LlamaCpp
from utils.prompts import (
    GENERATE_CRITICAL_POSTS,
    GENERATE_MODERATE_POSTS,
    GENERATE_SAFE_POSTS,
)


def generate_posts_json(llm_configs: tuple | list[tuple], output_file: str):
    """Crea posts aleatorios de diversas categorías usando los LLMs deseados."""
    if isinstance(llm_configs, tuple):
        llm_configs = [llm_configs]

    prompts_dict = {
        "critical": GENERATE_CRITICAL_POSTS,
        "moderate": GENERATE_MODERATE_POSTS,
        "safe": GENERATE_SAFE_POSTS,
    }

    if Path(output_file).exists():
        output_json = json.loads(Path(output_file).read_text())
    else:
        output_json = {}

    for llm_config in llm_configs:
        llm_path, gpu_layers = llm_config
        llm = LlamaCpp(
            model_path=llm_path,
            n_gpu_layers=gpu_layers,
            n_ctx=1024,
            temperature=0.7,
            max_tokens=1024,
            top_p=0.95,
            verbose=False,
        )
        llm_name = Path(llm_path).stem
        output_json[llm_name] = {"outputs": []}
        for prompt_key, prompt in prompts_dict.items():
            llm_output = llm.invoke(prompt)
            try:
                decoded_output = json.loads(llm_output)
                output_json[llm_name]["outputs"].append({prompt_key: decoded_output})
            except json.JSONDecodeError:
                output_json[llm_name]["outputs"].append({prompt_key: llm_output})

        llm.client.reset()
        llm.client.set_cache(None)
        del llm

        Path(output_file).write_text(json.dumps(output_json))


def posts_json_to_csv(json_file: str, csv_file: str):
    """Convierte el JSON con los posts generados a formato CSV."""
    output_json = json.loads(Path(json_file).read_text())
    data = []
    for llm in output_json.keys():
        for category in output_json[llm].keys():
            try:
                posts = output_json[llm][category]["Posts"]
                data += [(post, category, llm) for post in posts]
            except Exception:  # noqa: S110
                pass

    posts_df = pd.DataFrame.from_records(data=data, columns=("post", "category", "llm"))
    posts_df.sample(frac=1).to_csv(csv_file, index=False)


def posts_csv_to_tweets_js(csv_file: str, tweets_js_file: str):
    """Convierte el CSV con los posts al fichero `tweets.js`."""
    posts_df = pd.read_csv(csv_file)
    tweets = []
    for post in posts_df["post"].tolist():
        tweets.append(
            {
                "tweet": {
                    "full_text": post,
                    "retweet_count": "0",
                    "favorite_count": "0",
                    "created_at": "Sat Jan 1 00:00:00 +0000 2050",
                }
            }
        )
    tweets_js = "window.YTD.tweets.part0 = " + json.dumps(tweets, indent=2)
    Path(tweets_js_file).write_text(tweets_js)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        llm_configs = [(os.getenv("LLM_PATH"), int(os.getenv("GPU_LAYERS")))]
    elif len(sys.argv) == 2:
        llm_configs = [(sys.argv[1], None)]
    elif len(sys.argv) == 3:
        llm_configs = [(sys.argv[1], int(sys.argv[2]))]
    else:
        print("Uso: python create_posts.py [path_to_gguf_file] [n_gpu_layers]")
        sys.exit()

    generate_posts_json(llm_configs, "posts.json")
    posts_json_to_csv("posts.json", "posts.csv")
    posts_csv_to_tweets_js("posts.csv", "tweets.js")
