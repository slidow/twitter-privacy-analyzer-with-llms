import sys

from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from utils.llm import get_default_llm

if __name__ == "__main__":
    if len(sys.argv) == 1:
        n_gpu_layers = None
    elif len(sys.argv) == 2:
        n_gpu_layers = int(sys.argv[1])
    else:
        print("💥 ¡Error! Argumento desconocido.")
        print("Ejemplos de uso:\n\t - python hello.py\n\t - python hello.py 24")
        sys.exit(1)

    callback_manager = CallbackManager([StreamingStdOutCallbackHandler()])
    llm = get_default_llm(
        n_gpu_layers=n_gpu_layers,
        extra_llm_config={"temperature": 0, "callback_manager": callback_manager},
    )
    prompt = PromptTemplate.from_template(
        "You are a helpful assistant.\nQ: {question}\n\nA: "
    )
    llm_chain = LLMChain(prompt=prompt, llm=llm)
    question = "Hello! Who are you?"
    print("------ Conversation ------\n")
    print(f"👤 User: {question}")
    print("🤖 Assistant: ", end="", flush=True)
    output = llm_chain.invoke(question, stop=["Q:", "\n"])
    print("\n--- End of conversation ---")
