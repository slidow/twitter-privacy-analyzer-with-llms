GENERATE_SAFE_POSTS = """
You are a social media user who is very concern about remainig anonymous on the internet.

Create 10 short posts that preserve user anonymity.
This includes: generic posts, commenting on current affairs, riddles, jokes, famous quotes, etc.
- Posts that share news articles or viral content without any personal commentary.
- Posts with memes or funny things without any context or personal information.
- Posts that contain quotes or inspirational messages without any mention of the author's name or background.

You will only respond with a JSON object with the key Posts and a list of the generated posts. Do not provide explanations.
"""

GENERATE_MODERATE_POSTS = """
You are a social media user who never shares sensitive data.

Create 10 short posts that might provide some clues about the user's identity, but not enough to definitively identify them.

This includes:
- Posts that contain partial names, which could be shared among multiple people.
- Posts that mention general areas or regions where the user lives without providing the exact address.
- Posts that discuss general topics related to the user's interests, hobbies, political views, or beliefs.
- Posts that reveal some information about the user without specifying any sensitive data, such as talking about their pet, telling something about a family member or friend without giving details of their name or location, etc.

You will only respond with a JSON object with the key Posts and a list of the generated posts. Do not provide explanations.
"""

GENERATE_CRITICAL_POSTS = """
You are an unconscious social media user with no interest in preserving the privacy on the internet.

Create 10 short posts that contain specific and detailed information that can very easily identify the user.

This includes:
- Posts that contain full names, especially if combined with other identifying details.
- Posts that contain personally identifiable information (PII), such as addresses, phone numbers, or government-issued identification numbers.
- Posts that mention specific personal details, such as the user's workplace, job position, or school/university they attend.
- Posts that contain metadata that can identify the user, such as IP address or GPS coordinates.

You will only respond with a JSON object with the key Posts and a list of the generated posts. Do not provide explanations.
"""

PRIVACY_ANALYSIS_TEMPLATE = """
You are an expert on Internet privacy.

You will receive a social media post from an user that wants to preserve the privacy on the internet. You must classify posts within the following three categories:
- SAFE: posts that don't include any identifiable information.
- MODERATE: posts that provide some clues about the user's identity, but not enough to definitively identify them.
- CRITICAL: posts that contain specific and detailed information that can very easily identify the user.

You will also write the reason why you have assigned the category to the post.

You will only respond with a JSON object with the keys "category" and "reason". Do not provide explanations.

Here you have some examples:
{examples}

Q: "{post}"
A: ```json
"""

PRIVACY_ANALYSIS_EXAMPLES = """
Q: "@user123 Sure, just text me at +1 (333) 444-5555."
A: ```json
{"category": "CRITICAL", "reason": "Post severely compromises privacy by revealing the user's phone number, which can be used for scamming, SIM swapping attacks, and other harmful purposes."}
```

Q: "I love the sound of rain falling on the roof."
A: ```json
{"category": "SAFE", "reason": "Post does not disclose any personally identifiable information or specific details about the user. It's a general statement about a personal preference."}
```

Q: "Just got back from a walk with Max! #DogLife"
A: ```json
{"category": "MODERATE", "reason": "Post slightly compromises privacy as it reveals the user's dog's name (Max), which could potentially be linked to the user, but it's not enough to definitively identify the user."}
```

Q: "Can't wait for my upcoming trip to Madrid next week! **Attached picture of a flight ticket showing the user's passport number**"
A: ```json
{"category": "CRITICAL", "reason": "Post severely compromises privacy by revealing the user's travel plans, location, and, most critically, their passport number through the attached picture."}
```

Q: "Really disappointed with the customer service from Microsoft. They need to step up their game and address these issues ASAP #CustomerServiceFail"
A: ```json
{"category": "SAFE", "reason": "Post does not compromises user privacy as it focuses on expressing dissatisfaction with a company's customer service without revealing any personal information."}
```

Q: "What happens at 456 Oak St. in Anytown?!? I always pass this street at this time and I have never seen so many people.... -_-"
A: ```json
{"category": "CRITICAL", "reason": "Post severely compromises privacy by disclosing user's regular location (456 Oak St. in Anytown) and routine, which can be exploited for potentially harmful purposes."}
```
"""
