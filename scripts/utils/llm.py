import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from langchain_community.llms import LlamaCpp


def get_llm(
    llm_path: str,
    n_gpu_layers: int | None,
    verbose: bool | None = None,
    llm_config: dict | None = None,
    extra_llm_config: dict | None = None,
) -> LlamaCpp:
    if llm_path is None:
        print(
            "💥 ¡Error! No se ha configurado la variable `LLM_PATH`. Por favor, "
            "descarga un modelo compatible y configúralo correctamente. Lee el "
            "fichero README para más información."
        )
        sys.exit(1)
    elif not Path(llm_path).exists():
        print(
            f"💥 ¡Error! No se ha encontrado el fichero:\n - {llm_path}\n\n"
            "Asegúrate que has introducido correctamente la ruta del modelo."
        )
        sys.exit(1)

    if llm_config is None:
        llm_config = {
            "n_ctx": 1024,
            "temperature": 0.7,
            "max_tokens": 768,
            "top_p": 0.95,
            "verbose": False,
            "stop": ["Question:", "Q:"],
            "seed": 1984,
        }
    if extra_llm_config is not None:
        llm_config.update(extra_llm_config)
    if verbose is not None:
        llm_config["verbose"] = verbose

    n_gpu_layers = n_gpu_layers or llm_config.get("n_gpu_layers")
    if n_gpu_layers is None:
        print(
            "⚠️ Al no haber configurado la variable `GPU_LAYERS` no se usará la GPU, "
            "por lo que las respuestas puede que sean un poco lentas. Si dispones de "
            "GPU, es recomendable que la utilices para obtener un mejor rendimiendo. "
            "Lee el fichero README para más información."
        )
    llm_config["n_gpu_layers"] = n_gpu_layers

    print("\rCargando modelo... ⏳", end="", flush=True)
    llm = LlamaCpp(
        model_path=llm_path,
        **llm_config,
    )
    layers_str = "" if n_gpu_layers is None else f", gpu_layers={n_gpu_layers}"
    print(f"\rCargando modelo... Listo! ({Path(llm_path).name}{layers_str}) ✅")
    return llm


def get_default_llm(
    n_gpu_layers: int | None = None, extra_llm_config: dict | None = None
):
    load_dotenv()
    return get_llm(
        llm_path=os.getenv("LLM_PATH"),
        n_gpu_layers=n_gpu_layers or int(os.getenv("GPU_LAYERS")),
        verbose=False,
        llm_config=None,
        extra_llm_config=extra_llm_config,
    )
