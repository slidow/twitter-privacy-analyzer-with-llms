import json
import sys
import traceback
from datetime import datetime
from pathlib import Path

import pandas as pd
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from tqdm import tqdm
from utils.llm import get_default_llm
from utils.prompts import PRIVACY_ANALYSIS_EXAMPLES, PRIVACY_ANALYSIS_TEMPLATE


def parse_tweets(tweets_path: str) -> dict:
    tweets_content = Path(tweets_path).read_text()
    lines = tweets_content.split("\n")
    if lines[0].startswith("window.YTD.tweets") and lines[0].endswith("["):
        lines[0] = "["
    try:
        tweets = json.loads("\n".join(lines))
    except json.JSONDecodeError:
        print(
            '💥 ¡Error! No se ha podido procesar el fichero "tweets.js". ¿Has hecho '
            "alguna modificación al fichero tras descomprimirlo?"
        )
        sys.exit(1)
    return tweets


def analyze_tweets(tweets_path: str):
    tweets = parse_tweets(tweets_path)
    total_tweets, processed_tweets = len(tweets), 0
    for tweet in tweets:
        if "privacy_category" not in tweet["tweet"]:
            break
        processed_tweets += 1

    if processed_tweets == total_tweets:
        print(
            'Este fichero "tweets.js" ya había sido procesado completamente. ¿Quieres '
            "volver a procesarlo de nuevo? Se borrarán los resultados anterioes."
        )
        user_input = input(
            'Escribe "CONFIRMAR" para borrar los resultados y '
            "volver a procesar el fichero: "
        )
        if user_input.upper() == "CONFIRMAR":
            print("\rBorrando resultados anteriores... ⏳", end="", flush=True)
            processed_tweets = 0
            for tweet in tweets:
                del tweet["tweet"]["privacy_category"]
                del tweet["tweet"]["privacy_category_reason"]
            print("\rBorrando resultados anteriores... Listo! ✅")
        else:
            print("❌ Se ha cancelado el proceso.")
            sys.exit(0)

    if processed_tweets > 0:
        print("🔎 Se ha detectado una ejecución en curso, seguiremos donde lo dejamos.")

    print("\n------- 📝 RESUMEN DEL ANÁLISIS PREVIO 📝 -------")
    print(f" - Tweets totales:                  {total_tweets}")
    print(f" - Tweets previamente analizados:   {processed_tweets}")
    print(f" - Tweets restantes por analizar:   {total_tweets - processed_tweets}")
    print("--------------------------------------------------\n")

    llm = get_default_llm()
    prompt = PromptTemplate.from_template(PRIVACY_ANALYSIS_TEMPLATE.strip())
    prompt = prompt.partial(examples=PRIVACY_ANALYSIS_EXAMPLES.strip())
    llm_chain = LLMChain(prompt=prompt, llm=llm)

    print(
        "\n🚀 ¡El análisis ya ha comenzado! Dependiendo del hardware de tu PC y del "
        "número de tweets a analizar, puede tardar un rato. Coge un café y relájate. ☕"
    )
    print(
        "Recuerda: Puedes detener la ejecución en cualquier momento pulsando Ctrl+C "
        "guardaremos los avances y podrás continuar en cualquier otro momento.\n"
    )
    try:
        for i in tqdm(range(processed_tweets, total_tweets)):
            tweet_content = tweets[i]["tweet"]["full_text"]
            llm_output = llm_chain.invoke({"post": tweet_content}, stop=["Q:"])["text"]
            json_output = llm_output
            if "```json" in llm_output:
                json_output = json_output.split("```json")[-1].split("```")[0].strip()
            json_output = "{" + json_output.split("{")[-1].split("}")[0].strip() + "}"
            try:
                output = json.loads(json_output)
                output = {k.lower(): v for k, v in output.items()}
            except json.JSONDecodeError:
                print("💥 ¡Error al parsear el output del LLM!\n")
                print("----- PROMPT -----")
                print(str(prompt.format(post=tweet_content)))
                print("\n----- OUTPUT -----")
                print(llm_output)
                print("------------------")

            category = output.get("category")
            reason = output.get("reason")
            if category is None or reason is None:
                print(f"💥 ¡Error en el output! Output = {output}")
            tweets[i]["tweet"]["privacy_category"] = category.upper()
            tweets[i]["tweet"]["privacy_category_reason"] = reason
    except KeyboardInterrupt:
        print(
            "\n🛑 Ejecución detenida. Por favor, espera unos segundos a que "
            "guardemos el progreso..."
        )
    except Exception:
        print("💥 ¡Error inesperado!")
        print(traceback.format_exc())
    finally:
        tweets_js_content = "window.YTD.tweets.part0 = " + json.dumps(tweets, indent=2)
        Path(tweets_path).write_text(tweets_js_content)
        data = []
        for tweet in tweets:
            if "privacy_category" not in tweet["tweet"]:
                continue
            else:
                data.append(
                    (
                        tweet["tweet"]["full_text"],
                        tweet["tweet"]["privacy_category"],
                        tweet["tweet"].get("privacy_category_reason", ""),
                    )
                )

        detail_df = pd.DataFrame.from_records(
            data=data, columns=["Tweet", "Category", "Reason"]
        )
        summary_df = pd.DataFrame.from_records(
            data=[
                ("Tweets totales", total_tweets),
                ("Tweets procesados", detail_df.shape[0]),
                ("Tweets pendientes", total_tweets - detail_df.shape[0]),
                ("", ""),
                (
                    "Tweets SAFE",
                    detail_df.loc[detail_df["Category"] == "SAFE"].shape[0],
                ),
                (
                    "Tweets MODERATE",
                    detail_df.loc[detail_df["Category"] == "MODERATE"].shape[0],
                ),
                (
                    "Tweets CRITICAL",
                    detail_df.loc[detail_df["Category"] == "CRITICAL"].shape[0],
                ),
            ],
            columns=["DESC.", "CONTEO"],
        )
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
        excel_path = Path(tweets_path).parent / f"analysis_result_{timestamp}.xlsx"
        writer = pd.ExcelWriter(excel_path, engine="xlsxwriter")
        summary_df.to_excel(writer, sheet_name="SUMMARY", index=False)
        detail_df.to_excel(writer, sheet_name="DETAIL", index=False)
        writer.close()
        if total_tweets == detail_df.shape[0]:
            print(f"\n✅ Resultados guardados\n - Excel: {excel_path}")
        else:
            print(
                "\n✅ Progreso guardado. Puedes reanudarlo cuando quieras ejecutando el "
                f"mismo comando. Resultados preliminares:\n - Excel: {excel_path}"
            )


if __name__ == "__main__":
    if len(sys.argv) == 1:
        tweets_path = Path(__file__).parents[1] / "data/tweets.js"
    elif len(sys.argv) == 2:
        tweets_path = Path(sys.argv[1])
        if not tweets_path.exists():
            print(
                f"💥 ¡Error! No se ha encontrado el fichero:\n - {tweets_path}\n\n"
                "Asegúrate que has introducido correctamente la ruta del fichero."
            )
            sys.exit(1)

    analyze_tweets(str(tweets_path))
