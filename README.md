# GUÍA DE USO

## Instalación

### 1. Instalación de Python

Para ejecutar esta herramienta es necesario tener Python instalado en tu sistema. Dependiendo de tu sistema operativo, deberás seguir unos u otros pasos.

Podremos verificar si Python está instalado en nuestro sistema abriendo un terminal y ejecutando el siguiente comando:

```bash
python --version
```

También debemos asegurarnos de que `pip` esté instalado (debería instalarse al instalar Python):

```bash
pip --version
```

Si alguno de estos comandos nos da un error, deberemos revisar dónde está el problema, ya que necesitaremos ambos comandos.


### 2. Instalación de `llama-cpp-python`

Esta herramienta utiliza `llama-cpp-python` para la ejecución de nuestras LLMs (Large Language Models) locales.

Si seguimos las instrucciones de su [página oficial](https://llama-cpp-python.readthedocs.io/en/latest/), la instalación es bastante sencilla.

Es importante que prestemos atención al "backend" que queremos instalar. Esto dependerá de nuestro sistema operativo y nuestro hardware.

En general podemos suponer lo siguiente:
- Si no tienes tarjeta gráfica, usa `OpenBLAS`
- Si tienes una tarjeta gráfica Nvidia, usa `cuBLAS (CUDA)`
- Si tienes una tarjeta gráfica AMD, usa `hipBLAS (ROCm)`
- Si tienes un Mac, usa `Metal`

### 3. Instalamos las dependencias de la herramienta

Para ello simplemente tendremos que ejecutar el siguiente comando desde la carpeta de este proyecto:

```bash
pip install -r requirements.txt
```

Si te da un error de "No se ha encontrado el fichero `requirements.txt`", significa que no estás en la ruta correcta en tu terminal.

Puedes cambiarte de ruta usando el comando `cd`, simplemente busca en tu explorador de archivos la ruta en la que hayas descargado este proyecto.

```bash
# Linux / Mac
cd /home/tu_usuario/.../twitter-privacy-analyzer
# Windows (terminal Powershell)
cd C:\Users\tu_usuario\...\twitter-privacy-analyzer
```

### 4. Instalación de la herramienta

A continuación, instalaremos la herramienta desde la carpeta del proyecto con el siguiente comando:

```bash
pip install -e .
```

## Guía de LLMs locales

Ahora tendremos que descargar el LLM que queramos (o que nos permita ejecutar nuestro hardware) y ya estaremos listos para usar nuestro "ChatGPT" local, gratuito y 100% privado. La mejor página para buscar LLMs open source es [HuggingFace](https://huggingface.co/).

Pero te aviso, el mundo de los LLMs locales es una madriguera inmensa. Existen cientos de LLMs que podremos descargar y utilizar, y cada día surgen nuevos modelos y versiones.

Como no quiero que te satures con toda la información que hay disponible, voy a tratar de darte la información mínima indispensable para empezar a probar LLMs en local.

### Hardware

- RAM / VRAM (tarjeta gráfica): Será lo que limite el tamaño de modelos que podamos usar.

  Cita de un usuario de Reddit:
  > An unquantized version of a model (The "original") consumes 2GiB of VRAM (or RAM) per 1B - So a 7B model would consume roughly 14GiB. Currently it's popular to use 4-Bit quantizations, which consumes 0.5GiB per 1B, so a 7B model consumes about 3.5 GiB. However, you need to add 500 - 2000 MiB for working memory and stuff

  *Nota*: Con el "original" se refiere al modelo F16 (16 bits de precisión), más abajo veremos que es esto de la precisión.

- CPU: Cuanto más potente sea nuestra CPU, mayor rendimiento (palabras / segundo) podrá procesar nuestro PC.

- GPU (tarjeta gráfica): Si además contamos con GPU, podremos aliviar un poco la carga de la CPU, mejorando aún más el rendimiento de nuestros LLMs.


### Formato del fichero

Existen múltiples formatos de ficheros de LLMs, cada uno con una extensión distinta.

A nosotros nos interesa el formato GGUF (`.gguf`).

### Número de parámetros

Cuando estemos buscando qué modelo utilizar habitualmente nos encontraremos: `3B`, `7B`, `13B`, `33B`, `70B`, etc... en el nombre del modelo. Esto nos indica el número de parámetros del modelo, por ejemplo: 7B son 7.000 millones de parámetros.

¿Y para qué sirve? Pues es lo que determina el tamaño del modelo. Un modelo 70B requerirá un hardware mucho más potente que un modelo 3B, pero generalmente los modelos más grandes funcionan mejor.

Mi recomendación es que intentemos ejecutar un modelo 7-13B, los modelos más grandes es poco probable que no los podamos ejecutar sin una máquina muy potente, y los modelos más pequeños funcionarán regular (los 3B pueden servir para tareas sencillas).

### Cuantización

Otra cosa que nos encontraremos cuando vayamos a descargar los modelos son los sufijos del estilo: `Q4_0`, `Q2_K`, `Q5_K_S`, `Q3_K_L`, `Q8_0`, etc. Esto nos indica la cuantización usada.

La técnica de cuantización consiste en reducir un poco la precisión del modelo para que ocupe menos espacio y sea más fácil utilizarlo en hardware modesto. Sería algo así como lobotomizar al LLM original. Es muy recomendable, ya que nos permite ejecutar modelos más grandes perdiendo un poco de calidad (aunque suele valer la pena).

- El numerito que acompaña la Q indica la precisión de los parámetros, por ejemplo: Q2 = 2 bits, Q8 = 8 bits.
- Los `_0` y `_1` creo que son versiones `legacy` y por tanto ya no son tan recomendables.
- Los sufijos `_K_S`, `_K_M` (o `_K`) y `_K_L` son los formatos más recientes de cuantización y los recomendables. El orden es el siguiente: L > M > S.

¡Qué lío! ¿Con cuál nos quedamos? Pues en general es buena idea apuntar a las cuantizaciones `Q4_K_M` - `Q5_K_L`, ya que suelen ser un buen balance entre calidad y rendimiento ([más info](https://github.com/ggerganov/llama.cpp/discussions/2094#discussioncomment-6351796)).

### Consejos

- Un buen punto de partida es ir a por un modelo 7B y cuantización Q4_K_M, por ejemplo [`OpenHermes-2.5-Mistral-7B-GGUF`](https://huggingface.co/TheBloke/OpenHermes-2.5-Mistral-7B-GGUF/tree/main). Esto debería consumir unos ~4 GB de memoria RAM.
- Si vemos que nuestro hardware puede sin problemas, es decir, que obtenemos un buen rendimiento y aún tenemos mucha memoria RAM/VRAM libre, podemos dar el salto a un modelo un poco más grande. Abajo pongo algunos modelos populares.
- Una vez que hayamos encontrado un modelo popular con un tamaño adecuado para nuestro PC, nuestra siguiente misión es empezar a mejorar nuestras técnicas de prompt engineering.
- *Nota: En la actualidad, los LLMs open source funcionan mejor si les hablamos en inglés. Pueden resolver tareas sencillas en español sin mucho problema, pero para tareas un poco más complejas puede ser conveniente probar en inglés.*

### Lista de modelos populares (Feb. 2024)

#### Genéricos:
- [S] [Dolphin-2.6-Phi-3B](https://huggingface.co/TheBloke/dolphin-2_6-phi-2-GGUF)
- [S] [StableLM-Zephyr-3B](https://huggingface.co/TheBloke/stablelm-zephyr-3b-GGUF)
- [M] [OpenHermes-2.5-Mistral-7B](https://huggingface.co/TheBloke/OpenHermes-2.5-Mistral-7B-GGUF)
- [M] [NeuralHermes-2.5-Mistral-7B](https://huggingface.co/TheBloke/NeuralHermes-2.5-Mistral-7B-GGUF)
- [M] [Dolphin-2.6-mistral-7B](https://huggingface.co/TheBloke/dolphin-2.6-mistral-7B-GGUF)
- [M] [CapybaraHermes-2.5-Mistral-7B](https://huggingface.co/TheBloke/CapybaraHermes-2.5-Mistral-7B-GGUF)
- [M] [Trinity-v1.2-7B](https://huggingface.co/janhq/trinity-v1.2-GGUF)
- [M+] [Nous-Hermes-2-SOLAR-10.7B](https://huggingface.co/TheBloke/Nous-Hermes-2-SOLAR-10.7B-GGUF)
- [L] [Nous-Hermes-2-Yi-34B](https://huggingface.co/NousResearch/Nous-Hermes-2-Yi-34B-GGUF)
- [XL] [Mixtral-8x7B-Instruct](https://huggingface.co/TheBloke/Mixtral-8x7B-Instruct-v0.1-GGUF)
- [XL] [Dolphin-2.7-mixtral-8x7b](https://huggingface.co/TheBloke/dolphin-2.7-mixtral-8x7b-GGUF)
- [XL] [Nous-Hermes-2-Mixtral-8x7B-DPO](https://huggingface.co/NousResearch/Nous-Hermes-2-Mixtral-8x7B-DPO-GGUF)


Mi recomendación es:
- No utilizar los modelos 3B a no ser que nuestros requisitos de hardware nos impidan utilizar otra cosa.
- El rango alrededor de los 7B es donde salen más modelos, y donde están los mejores _calidad/rendimiento_.
- **OpenHermes-2.5-Mistral-7B** es el padre de toda la familia de modelos Hermes. Existen variantes como **NeuralHermes-2.5-Mistral-7B** o **CapybaraHermes-2.5-Mistral-7B**.
- En la actualidad, **Nous-Hermes-2-SOLAR-10.7B** sería uno de los mejores en este rango.
- En Huggingface, tenéis un [leaderboard](https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard) que puede ser útil para comparar modelos.
  - No os fieis mucho de los "mejores" modelos, ya que estos son habitualmente refritos de refritos que han sido especializados en tener mejor puntuación en la clasificación, pero no significa que sean mejores en la práctica.
  - Yo el leaderboard lo utilizo para buscar modelos específicos y compararlos con otros.
- El siguiente salto de tamaño sería irnos a **Nous-Hermes-2-Yi-34B**, pero es un poco mejor que **Nous-Hermes-2-SOLAR-10.7B** y tiene unos requisitos mucho más altos. Sólo me iría a este modelo si viera que mi PC va muy sobrado con el anterior.
- Y por último, nos iríamos a la familia de modelos Mixtral. Son modelos muy potentes (hasta se podría decir que pueden competir con los modelos de OpenAI) y por tanto tienen unos requisitos muy altos para ejecutarlos con un rendimiento decente. Aquí me quedaría con **Nous-Hermes-2-Mixtral-8x7B-DPO** o **Mixtral-8x7B-Instruct**.

#### Especializados en Programación:
- [Deepseek-Coder-6.7B-Instruct](https://huggingface.co/TheBloke/deepseek-coder-6.7B-instruct-GGUF)
- [Deepseek-Coder-33B-Instruct](https://huggingface.co/TheBloke/deepseek-coder-33B-instruct-GGUF)

#### Multimodal
No sólo aceptan texto, por ejemplo puedes preguntarle sobre una imagen.
- [LLaVa-1.6-Mistral-7B](https://huggingface.co/cjpais/llava-1.6-mistral-7b-gguf)
- [LLaVa-v1.6-Yi-34B](https://huggingface.co/cjpais/llava-v1.6-34B-gguf)
- [BakLLaVA-1](https://huggingface.co/abetlen/BakLLaVA-1-GGUF)

## Uso de esta herramienta

Esta herramienta se basa en [`llama-cpp-python`](https://abetlen.github.io/llama-cpp-python/) y [`langchain`](https://python.langchain.com/docs/get_started/introduction), así que para más información consultad sus guías.

### Configuración del fichero `.env`

Si no lo tenemos ya creado, deberemos crear un fichero llamado `.env` con el siguiente contenido:

```bash
# Ubicación del modelo que hayas descargado
LLM_PATH=/home/tu_usuario/.../tu_modelo.gguf

# Números de capas que quieras que procese tu GPU
GPU_LAYERS=24
```

En la variable `LLM_PATH` deberemos indicar la ubicación del modelo que hemos descargado.
- En Linux / Mac la ruta será algo como: `/home/tu_usuario/.../tu_modelo.gguf`.
- En Windows se parecerá a: `C:\Users\tu_usuario\...\tu_modelo.gguf`.

La variable `GPU_LAYERS` indica cuántas capas del modelo queremos que nuestra GPU utilice.
- Si elegimos -1, se intentarán cargar todas las capas y si el modelo es demasiado grande para nuestra tarjeta gráfica, nos dará un error.
- Si elegimos 0 o borramos esa línea, sólo se utilizará la CPU. Si no tenemos tarjeta gráfica o hemos instalado `llama-cpp-python` con OpenBLAS, deberemos usar esta opción.
- Si elegimos otro número, por ejemplo 24, se intentarán cargar ese número de capas en la GPU. Lo ideal sería que para un modelo concreto, vayamos probando cuál es el número máximo de capas que nos permite ejecutar nuestra GPU.
  - Por ejemplo en mi caso `neuralhermes-2.5-mistral-7b.Q8_0.gguf` me permite usar 28 capas, mientras que `nous-hermes-2-solar-10.7b.Q5_K_M.gguf` me permite 45.
  - Esto también puede variar ligeramente si tenemos otros procesos consumiendo recursos de la gráfica en paralelo.

### Ejecución de la herramienta

#### Comprobación de instalación

Para comprobar que todo se haya instalado correctamente ejecutaremos el siguiente comando:

```bash
python scripts/hello.py
```

También podemos pasarle un número para indicarle el número de GPU layers. Este script puede ser util para probar cuál es el GPU layers más grande que no nos devuelve error de memoria.

```bash
python scripts/hello.py 30
```

#### Análisis de tweets

- Si queremos ejecutar esta herramienta para analizar nuestros tweets, primero deberemos ir a Twitter y solicitar el archivo con todos nuestros datos. Este proceso puede tardar unas horas o días y cuando el archivo esté listo nos aparecerá en una notificación. Luego sólo tendremos que descomprimir el fichero ZIP y el fichero con nuestros tweets está en la carpeta "data" (`data/tweets.js`).

- Para analizar nuestro fichero `tweets.js` y encontrar qué tweets pueden ser perjudiciales para nuestra privacidad, ejecutaremos el siguiente comando:

```bash
python src/privacy_analysis.py /home/tu_usuario/.../tweets.js
```

- Si no tenemos el archivo con nuestros datos de Twitter listo, no pasa nada. Podrás ejecutar igualmente el script con un fichero `tweets.js` de ejemplo (el que se encuentra en la carpeta data de este proyecto). Simplemente debes ejecutar el comando anterior sin indicar ninguna ruta:

```bash
python src/privacy_analysis.py
```

El script clasificará cada tweet en las siguientes categorías:
- SAFE: no se ha encontrado ningún problema de privacidad
- MODERATE: se ha encontrado algún problema menor de privacidad
- CRITICAL: se ha encontrado algún problema grave de privacidad

Además el LLM generará un motivo de por qué ha elegido la categoría.

Estas categorías se añadirán al `tweets.js`, de esta forma si queremos interrumpir la ejecución del script (pulsando `Ctrl+C`), podremos continuar con el análisis en otro momento por donde lo habíamos dejado.

También se generará el fichero Excel `analysis_result_{fecha_hora}.xlsx` (en la misma carpeta que `tweets.js`). Este fichero tendrá dos pestañas:
- SUMMARY: Contiene el resumen del análisis.
- DETAIL: Contiene el detalle de todos los tweets analizados.
